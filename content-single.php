<?php
/**
 * @package Cloud3Dots LodgeXYZ
 */
?>
<article id="post-<?php the_ID(); ?>" <?php post_class('single-post'); ?>>


    <header class="entry-header">
        <h2 class="single_title"><?php the_title(); ?></h2>
    </header><!-- .entry-header -->

      <div class="postmeta">
        <?php
          $author_name = get_the_author_meta('nickname');
          if ( empty($author_name) ) {
            $author_name = get_the_author_meta('display_name');
          }
          if ( !empty($author_name) ) {
        ?>
            <div class="post-author">By <?php echo get_the_author_meta('nickname'); ?> &nbsp;|&nbsp; </div><!-- post-date -->
        <?php
          }
        ?>
            <div class="post-date"><?php echo get_the_date(); ?></div><!-- post-date -->
            <div class="post-comment"> &nbsp;|&nbsp; <a href="<?php comments_link(); ?>"><?php comments_number(); ?></a></div>
            <div class="clear"></div>
    </div><!-- postmeta -->

    <?php
        if (has_post_thumbnail()) {
            echo '<div class="post-thumb">';
            the_post_thumbnail();
            echo '</div>';
        }
        ?>

    <div class="entry-content">


        <?php the_content(); ?>
        <?php
        wp_link_pages(array(
            'before' => '<div class="page-links">' . esc_attr__('Pages:', 'lodgexyz'),
            'after'  => '</div>',
        ));
        ?>
        <div class="postmeta">
            <div class="post-categories"><?php the_category(', '); ?></div>
            <div class="post-tags"><?php the_tags(', '); ?> </div>
            <div class="clear"></div>
        </div><!-- postmeta -->
    </div><!-- .entry-content -->

    <footer class="entry-meta">
      <?php edit_post_link(esc_attr__('Edit', 'lodgexyz'), '<span class="edit-link">', '</span>'); ?>
    </footer><!-- .entry-meta -->

</article>
