=== LodgeXYZ ===

Contributors: @jfederico
Tags: custom-background, custom-logo, custom-menu, custom-colors, featured-images, threaded-comments, translation-ready

Requires at least: 4.5
Tested up to: 5.3.2
Stable tag: 0.1.12
License: GNU General Public License v3 or later
License URI: licence.txt

A Responsive WordPress Theme called LodgeXYZ.

== Description ==

LodgeXYZ clean, minimal, simple and adaptable responsive multipurpose WordPress theme customized for masonic lodges can also be used by any NGO/NPO. Compatible with nextgen gallery, WooCommerce and Contact Form 7 it can be used as an E-Commerce as well.

== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload Theme and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

LodgeXYZ includes support for Infinite Scroll in Jetpack.

== Changelog ==

= 0.1.11 - Dec 21 2020 =
* Added author to posts

= 0.1.10 - Dec 31 2019 =
* Added rudimentary filter for Posts shown in front page

= 0.1.9 - Jan 05 2020 =
* Fixed issue with front-end dashicons

= 0.1.8 - Dec 31 2019 =
* Updates to images by default

= 0.1.7 - Jun 21 2019 =
* Initial release

== Credits ==

* Forked from Naturo Lite WordPress Theme https://www.sktthemes.net/shop/naturo_lite/, Copyright (c) 2015 SKT Themes, [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
* Uses Font Cloud3Dots https://gitlab.com/cloud3dots/font-cloud3dots/, Copyright (c) 2019 Cloud3Dots, [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html)
