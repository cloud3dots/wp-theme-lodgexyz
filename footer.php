<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package Cloud3Dots LodgeXYZ
 */
?>
<div id="footer-wrapper">
    <div class="container">
        <div class="cols-4 widget-column-1">
            <h5><?php echo esc_attr(get_theme_mod('about_title', __('About Us', 'lodgexyz'))); ?></h5>
            <?php echo esc_attr(get_theme_mod('about_description', __('Sed suscipit mauris nec mauris vulputate, a posuere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod.Nam metus lorem, hendrerit quis ante eget, lobortis elementum neque. Aliquam in ullamcorper quam. Integer euismod ligula in mauris vehic.', 'lodgexyz'))); ?>
        </div>

        <div class="cols-4 widget-column-2">
            <h5><?php esc_attr_e('Recent Posts', 'lodgexyz'); ?></h5>
            <?php $args = array( 'posts_per_page' => 2, 'post__not_in' => get_option('sticky_posts'), 'orderby' => 'date', 'order' => 'desc' );
                  $the_query = new WP_Query($args);
                    ?>
            <?php while ($the_query->have_posts()) :  $the_query->the_post(); ?>
                  <div class="recent-post">
                      <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('thumbnail'); ?></a>
                      <a href="<?php the_permalink(); ?>"><h6><?php the_title(); ?></h6></a>
                      <?php echo wp_kses_post(lodgexyz_short_the_content(apply_filters('the_content', get_the_content()))); ?>
                  </div>
            <?php endwhile; ?>
        </div>

        <div class="cols-4 widget-column-3">
          <?php if ('' != get_theme_mod('contact_sch1') || '' != get_theme_mod('contact_sch2') ||
                    '' != get_theme_mod('contact_add1') || '' != get_theme_mod('contact_add2') ||
                    '' != get_theme_mod('contact_no') || '' != get_theme_mod('contact_mail')) {
                        ?>
                   <h5><?php esc_attr_e('Contact Info', 'lodgexyz'); ?></h5>
          <?php
                    } ?>
          <?php if ('' != get_theme_mod('contact_sch1') || '' != get_theme_mod('contact_sch2')) {
                        ?>
                   <p>
                     <?php echo esc_attr(get_theme_mod('contact_sch1', __('Meets 4th Fri, 7:30pm', 'lodgexyz'))); ?><br>
                     <?php echo esc_attr(get_theme_mod('contact_sch2', __('Except Jun, Jul and Aug', 'lodgexyz'))); ?>
                   </p>
			    <?php
                    } ?>
          <?php if ('' != get_theme_mod('contact_add1') || '' != get_theme_mod('contact_add2')) {
                        ?>
                   <p>
                     <?php echo esc_attr(get_theme_mod('contact_add1', __('Masonic Centre', 'lodgexyz'))); ?><br>
                     <?php echo esc_attr(get_theme_mod('contact_add2', __('69 Keas Rd., Athens, Chalandri 15234, Grece', 'lodgexyz'))); ?>
                   </p>
			    <?php
                    } ?>
          <?php if ('' != get_theme_mod('contact_no') || '' != get_theme_mod('contact_mail')) {
                        ?>
              <div class="phone-no"><strong><?php if ('' != get_theme_mod('contact_no')) {
                            ?><?php esc_attr_e('Phone:', 'lodgexyz');
                        } ?></strong>
			        <?php echo esc_attr(get_theme_mod('contact_no', '+30-2106019311')); ?><br  />
              <strong> <?php if ('' != get_theme_mod('contact_mail')) {
                            esc_attr_e('Email:', 'lodgexyz');
                        } ?></strong> <a href="mailto:<?php echo sanitize_email(get_theme_mod('contact_mail', 'contact@example.com')); ?>"><?php echo esc_attr(get_theme_mod('contact_mail', 'contact@example.com')); ?></a></div>
          <?php
                    } ?>

        <div class="clear"></div>
        <div class="social-icons">
					<?php if ('' != get_theme_mod('fb_link')) {
                        ?>
                    <a title="facebook" class="fa fa-facebook fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('fb_link', '#facebook')); ?>"></a>
          <?php
                    } ?>

          <?php if ('' != get_theme_mod('twitt_link')) {
                        ?>
                    <a title="twitter" class="fa fa-twitter fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('twitt_link', '#twitter')); ?>"></a>
          <?php
                    } ?>

          <?php if ('' != get_theme_mod('gplus_link')) {
                        ?>
                    <a title="google-plus" class="fa fa-google-plus fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('gplus_link', '#gplus')); ?>"></a>
          <?php
                    }?>

          <?php if ('' != get_theme_mod('linked_link')) {
                        ?>
                    <a title="linkedin" class="fa fa-linkedin fa-1x" target="_blank" href="<?php echo esc_url(get_theme_mod('linked_link', '#linkedin')); ?>"></a>
          <?php
                    } ?>
                </div>
                </div><!--end .widget-column-4-->
        <div class="clear"></div>
    </div><!--end .container-->

    <div class="copyright-wrapper">
        <div class="container">
            <?php if (get_theme_mod('copyright_rights', true)) {
                        ?>
                <div class="copyright-txt"><?php printf(esc_html__(__('Copyright &copy; %1$s %2$s. All Rights Reserved.', 'lodgexyz')), date("Y"), get_bloginfo('name')); ?></div>

            <?php
                    } ?>
            <?php if (get_theme_mod('copyright_credits', true)) {
                        ?>
                <div class="design-by"><?php printf(esc_html__('Theme %1$s %2$s', 'lodgexyz'), 'by', '<a href="'.esc_url(CLOUD3DOTS_URL).'"><i class="c3d c3d-cloud3dots c3d-2x"></i>Cloud3Dots</a>'); ?></div>
            <?php
                    } ?>
        </div><!--end .container-->
        <div class="clear"></div>
    </div><!--end .copyright-wrapper-->
</div><!--end .footer-wrapper-->
<?php wp_footer(); ?>
</body>
</html>
