<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div class="container">
 *
 * @package Cloud3Dots LodgeXYZ
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>">
<?php wp_head(); ?>

</head>

<body <?php body_class(''); ?>>
<div class="header">
  <div class="signin_wrap">
    <div class="container">
      <div class="right">
      <?php if (is_user_logged_in()) { ?>
        <?php $current_user = wp_get_current_user(); ?>
        <span>
          Welcome, <?php echo $current_user->display_name; ?>! <a href="<?php echo wp_logout_url(home_url()); ?>" title="<?php esc_attr_e('Sign Out', 'lodgexyz'); ?>">
            <i class="fa fa-sign-out"></i><?php esc_attr_e('Sign Out', 'lodgexyz'); ?></a>
        </span>
      <?php } else { ?>
        <span>Welcome, visitor! <a href="<?php echo wp_login_url(home_url(add_query_arg(array(), $wp->request))); ?>" title="<?php esc_attr_e('Sign In', 'lodgexyz'); ?>">
          <i class="fa fa-user"></i><?php esc_attr_e('Sign In', 'lodgexyz'); ?></a></span>
          <?php if (get_option('users_can_register')) { ?>
            <span><a href="<?php echo site_url('/wp-login.php?action=register&redirect_to=' . get_permalink()); ?>" title="<?php esc_attr_e('Sign Up', 'lodgexyz'); ?>"><i class="fa fa-user-plus"></i><?php esc_attr_e('Sign Up', 'lodgexyz'); ?></a></span>
          <?php } ?>
      <?php } ?>
      </div>
      <div class="clear"></div>
    </div>
  </div><!--end signin_wrap-->

<section id="wrapmenu">

        <div class="header-inner">
          <?php if ('' != get_theme_mod('custom_logo')) {
        ?>
                <div class="logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                            <?php
                                $custom_logo_id = get_theme_mod('custom_logo');
        $custom_logo_url = wp_get_attachment_image_url($custom_logo_id, 'full');
        echo '<img src="' . esc_url($custom_logo_url) . '" alt="">'; ?>
                        </a>
                </div><!-- logo -->
          <?php
    } ?>

          <?php if (display_header_text()) {
        ?>

          <?php     if ('' == get_theme_mod('custom_logo')) {
            ?>

                <div class="logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                                <span class="c3d c3d-blue-lodge"></span>
                        </a>
                </div><!-- logo -->
          <?php
        } ?>

                <div class="logo">
                        <a href="<?php echo esc_url(home_url('/')); ?>">
                                <h1><?php bloginfo('name'); ?></h1>
                                <span class="tagline"><?php bloginfo('description'); ?></span>
                        </a>
                </div><!-- logo -->
          <?php
    } ?>
                <div class="toggle">
                <a class="toggleMenu" href="#"><?php esc_attr_e('Menu', 'lodgexyz'); ?></a>
                </div><!-- toggle -->
                <div class="nav">
                    <?php wp_nav_menu(array('theme_location' => 'primary')); ?>
                </div><!-- nav --><div class="clear"></div>
                    </div><!-- header-inner -->
</div><!-- header -->
</section><!--end section#wrapmenu-->

<?php if (is_front_page() && is_home() && get_theme_mod('announcement_enabled', false)) { ?>
<section id="wrapannouncements">
  <div class="container">
    <div class="center">
      <?php echo esc_attr(get_theme_mod('announcement_text', __("Howdy, check this out!", 'lodgexyz'))); ?>
    </div>
  </div><!--end section#wrapannouncements.container-->
</section><!--end section#wrapannouncements-->
<?php } ?>

<?php if (is_front_page() && is_home()) { ?>
<section id="home_slider">
    <?php
        $sldimages = '';
        $sldimages = array(
                        '1' => get_template_directory_uri().'/assets/images/slides/slider1.png',
                        '2' => get_template_directory_uri().'/assets/images/slides/slider2.png',
                        '3' => get_template_directory_uri().'/assets/images/slides/slider3.png',
            ); ?>

    <?php
        $slAr = array();
        $m = 0;
        for ($i=1; $i<4; $i++) {
            if (get_theme_mod('slider_image'.$i, $sldimages[$i]) != "") {
                $imgSrc 	= get_theme_mod('slider_image'.$i, $sldimages[$i]);
                $imgTitle	= get_theme_mod('slider_title'.$i);
                $imgDesc	= get_theme_mod('slider_desc'.$i);
                $imgLink	= get_theme_mod('slider_link'.$i);
                if (strlen($imgSrc) > 2) {
                    $slAr[$m]['image_src'] = get_theme_mod('slider_image'.$i, $sldimages[$i]);
                    $slAr[$m]['image_title'] = get_theme_mod('slider_title'.$i);
                    $slAr[$m]['image_desc'] = get_theme_mod('slider_desc'.$i);
                    $slAr[$m]['image_link'] = get_theme_mod('slider_link'.$i);
                    $m++;
                }
            }
        }
        $slideno = array();
        if ($slAr > 0) {
            $n = 0; ?>
                <div class="slider-wrapper theme-default">
                  <?php if (get_theme_mod('slider_overlay_enabled') == '1') {
                ?>
                  <div id="slider-overlay"></div>
                  <?php
            } ?>
                  <div id="slider" class="nivoSlider">
                <?php
                foreach ($slAr as $sv) {
                    $n++; ?><img src="<?php echo esc_url($sv['image_src']); ?>" alt="<?php echo esc_attr($sv['image_title']); ?>" title="<?php echo esc_attr('#slidecaption'.$n) ; ?>" /><?php
                    $slideno[] = $n;
                } ?>
                </div><?php
                foreach ($slideno as $sln) {
                    ?>
                    <div id="slidecaption<?php echo esc_attr($sln); ?>" class="nivo-html-caption">
                        <div class="slider_info">
                            <h2><a href="<?php echo esc_url(get_theme_mod('slider_link'.$sln, '#link'.$sln)); ?>"><?php echo esc_attr(get_theme_mod('slider_title'.$sln, __('Celebrating 300 Years of Freemasonry', 'lodgexyz').$sln)); ?></a></h2>
                            <p><?php echo esc_attr(get_theme_mod('slider_desc'.$sln, __("It's 300 years since four London Lodges met to establish the world's first Grand Lodge for Freemasons. Today there are over 6.5 million freemasons worldwide.", 'lodgexyz').$sln)); ?></p>
                        </div>
                    </div><?php
                } ?>
                </div>
                <div class="clear"></div><?php
        } ?>
</section><!--end section#home_slider-->
<div class="clear"></div>

<section id="wrapwidgets">
  <div class="container">
    <div class="center">

      <!--schedule widget widget_icon_box-->
      <?php if ('' != get_theme_mod('contact_sch1') || '' != get_theme_mod('contact_sch2')) {
            ?>
      <div class="widget widget-icon-box">
          <div class="widget-icon-box">
            <i class="dashicons dashicons-clock"></i>
            <div class="widget-icon-box__text">
              <h4 class="widget-icon-box__title"><?php echo esc_attr(get_theme_mod('contact_sch1', 'Meets 4th Fri, 7:30pm', 'lodgexyz')); ?></h4>
              <span class="widget-icon-box__subtitle"><?php echo esc_attr(get_theme_mod('contact_sch2', 'Except Jun, Jul and Aug', 'lodgexyz')); ?></span>
            </div>
          </div>
      </div><!--end schedule widget widget_icon_box-->
      <?php
        } ?>

      <!--address widget widget_icon_box-->
      <?php if ('' != get_theme_mod('contact_add1') || '' != get_theme_mod('contact_add2')) {
            ?>
      <div class="widget widget-icon-box">
          <div class="widget-icon-box">
            <i class="dashicons dashicons-building"></i>
            <div class="widget-icon-box__text">
              <h4 class="widget-icon-box__title">
                <a href="http://maps.google.com/?q=<?php echo esc_attr(get_theme_mod('contact_add1', 'Athens Masonic Centre', 'lodgexyz')); ?>+<?php echo esc_attr(get_theme_mod('contact_add2', '69 Keas Rd., Athens, ON', 'lodgexyz')); ?>">
                  <?php echo esc_attr(get_theme_mod('contact_add1', 'Athens Masonic Centre', 'lodgexyz')); ?>
                </a>
              </h4>
              <span class="widget-icon-box__subtitle">
                <a href="http://maps.google.com/?q=<?php echo esc_attr(get_theme_mod('contact_add1', 'Athens Masonic Centre', 'lodgexyz')); ?>+<?php echo esc_attr(get_theme_mod('contact_add2', '69 Keas Rd., Athens, ON', 'lodgexyz')); ?>">
                  <?php echo esc_attr(get_theme_mod('contact_add2', '69 Keas Rd., Athens, ON', 'lodgexyz')); ?>
                </a>
              </span>
            </div>
          </div>
      </div><!--end address widget widget_icon_box-->
      <?php
        } ?>

      <!--contact widget widget_icon_box-->
      <?php if ('' != get_theme_mod('contact_no') || '' != get_theme_mod('contact_mail')) {
            ?>
      <div class="widget widget-icon-box">
          <div class="widget-icon-box">
            <i class="dashicons dashicons-phone"></i>
            <div class="widget-icon-box__text">
              <h4 class="widget-icon-box__title">
                <a href="tel:<?php echo get_theme_mod('contact_no', '+30-2106019311'); ?>"><?php echo esc_attr(get_theme_mod('contact_no', '+30-2106019311', 'lodgexyz')); ?></a>
              </h4>
              <span class="widget-icon-box__subtitle">
                <a href="mailto:<?php echo sanitize_email(get_theme_mod('contact_mail', 'contact@example.com')); ?>"><?php echo esc_attr(get_theme_mod('contact_mail', 'contact@example.com')); ?></a>
              </span>
            </div>
          </div>
      </div><!--end contact widget widget_icon_box-->
      <?php
        } ?>

    </div><!--end section#wrapwidgets.center-->
    <div class="clear"></div>
  </div><!--end section#wrapwidgets.container-->
</section><!--end section#wrapwidgets-->

<?php
  /* Home Featured Page */
  if (get_theme_mod('page_featured_enabled', true)) {
      ?>
  <section id="wrapfirst">
    <div class="container">
      <?php $bxquery = new WP_query('page_id='.get_theme_mod('page_featured_setting', true)); ?>
      <?php
      if ($bxquery->have_posts()) {
          ?>
        <?php $bxquery->the_post(); ?>
          <center>
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            <a href="<?php the_permalink(); ?>"><h1 class="entry-title"><?php the_title(); ?></h1></a>
          </center>
          <?php echo wp_kses_post(lodgexyz_short_the_content(apply_filters('the_content', get_the_content()), esc_attr(get_theme_mod('page_featured_word_limit', 160)))); ?>
          <div class="center">
            <a href="<?php the_permalink(); ?>"><p class="CallToAction"><?php esc_attr_e(get_theme_mod('page_featured_button', __('Learn More', 'lodgexyz')), 'lodgexyz'); ?></p></a>
          </div>
      <?php
      } ?>
      <div class="clear"></div>
    </div><!--end section#wrapfirst.container-->
  </section><!--end section#wrapfirst-->
  <?php
  } ?>

<section id="wrapsecond">
        <div class="container">
            <?php if (get_theme_mod('hide_boxes') == '') {
      ?>
                <div class="services-wrap">

<?php
    /* Home Four Boxes */
    for ($bx=1; $bx<5; $bx++) {
        ?>
      <?php if (get_theme_mod('page-setting'.$bx)) {
            ?>
        <?php $bxquery = new WP_query('page_id='.get_theme_mod('page-setting'.$bx, true)); ?>
        <?php while ($bxquery->have_posts()) : $bxquery->the_post(); ?>
          <div class="one_fourth <?php if ($bx%4==0) {
                ?>last_column<?php
            } ?>">
            <div class="content">
            <?php if ('' != get_theme_mod('page-icon'.$bx, true)) {
                ?>
              <i class="<?php echo esc_attr(get_theme_mod('page-icon'.$bx, true)); ?>"></i>
            <?php
            } else {
                ?>
              <i class="fa fa-<?php echo lodgexyz_get_fa_icon('feature-box-' . esc_attr($bx)); ?>"></i>
            <?php
            } ?>
            <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
            <?php echo wp_kses_post(lodgexyz_short_the_content(apply_filters('the_content', get_the_content()), esc_attr(get_theme_mod('page_boxes_word_limit', 50)))); ?>
            </div>
            <div class="action">
              <a href="<?php the_permalink(); ?>"><p class="ReadMore"><?php esc_attr_e('Read More', 'lodgexyz'); ?></p></a>
            </div>
          </div>
          <!-- feature-box -->
          <?php if ($bx%4==0) {
                ?>
            <div class="clear"></div>
          <?php
            } ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
      <?php
        } else {
            ?>
        <!-- feature-box -->
        <?php if ($bx%4==0) {
                ?>
          <div class="clear"></div>
        <?php
            } ?>
            <?php
        } ?>
        <?php
    }
      /* Home Four Boxes */
    ?>
               </div><!-- services-wrap-->

               <?php
  } ?>

              <div class="clear"></div>
            </div><!-- container -->
       </section>
       <div class="clear"></div>
<?php
    } elseif (is_front_page()) {
        ?>
<section id="home_slider">
    <?php
            $sldimages = '';
        $sldimages = array(
                        '1' => get_template_directory_uri().'/assets/images/slides/slider1.jpg',
                        '2' => get_template_directory_uri().'/assets/images/slides/slider2.jpg',
                        '3' => get_template_directory_uri().'/assets/images/slides/slider1.jpg',
            ); ?>

    <?php

            $m = 0;
        for ($i=1; $i<4; $i++) {
            if (get_theme_mod('slider_image'.$i, $sldimages[$i]) != "") {
                $imgSrc 	= get_theme_mod('slider_image'.$i, $sldimages[$i]);
                $imgTitle	= get_theme_mod('slider_title'.$i);
                $imgDesc	= get_theme_mod('slider_desc'.$i);
                $imgLink	= get_theme_mod('slider_link'.$i);
                if (strlen($imgSrc) > 2) {
                    $slAr[$m]['image_src'] = get_theme_mod('slider_image'.$i, $sldimages[$i]);
                    $slAr[$m]['image_title'] = get_theme_mod('slider_title'.$i);
                    $slAr[$m]['image_desc'] = get_theme_mod('slider_desc'.$i);
                    $slAr[$m]['image_link'] = get_theme_mod('slider_link'.$i);
                    $m++;
                }
            }
        }
        $slideno = array();
        if ($slAr > 0) {
            $n = 0; ?>
                <div class="slider-wrapper theme-default"><div id="slider" class="nivoSlider">
                <?php
                foreach ($slAr as $sv) {
                    $n++; ?><img src="<?php echo esc_url($sv['image_src']); ?>" alt="<?php echo esc_attr($sv['image_title']); ?>" title="<?php echo esc_attr('#slidecaption'.$n) ; ?>" /><?php
                    $slideno[] = $n;
                } ?>
                </div><?php
                foreach ($slideno as $sln) {
                    ?>
                    <div id="slidecaption<?php echo esc_attr($sln); ?>" class="nivo-html-caption">
                        <div class="slider_info">
                            <h2><a href="<?php echo esc_url(get_theme_mod('slider_link'.$sln, '#link'.$sln)); ?>"><?php echo esc_attr(get_theme_mod('slider_title'.$sln, __('Celebrating 300 Years of Freemasonry', 'lodgexyz').$sln)); ?></a></h2>
                            <p><?php  echo esc_attr(get_theme_mod('slider_desc'.$sln, __("It's 300 years since four London Lodges met to establish the world's first Grand Lodge for Freemasons. Today there are over 6.5 million freemasons worldwide.", 'lodgexyz').$sln)); ?></p>
                        </div>
                    </div><?php
                } ?>
                </div>
                <div class="clear"></div><?php
        } ?>
        </section>
<div class="clear"></div>
<section id="wrapsecond">
        <div class="container">
            <?php if (get_theme_mod('hide_boxes') == '') {
            ?>
                <div class="services-wrap">

<?php
        /* Home Four Boxes */
        for ($bx=1; $bx<5; $bx++) {
            ?>
        <?php if (get_theme_mod('page-setting'.$bx)) {
                ?>
        <?php $bxquery = new WP_query('page_id='.get_theme_mod('page-setting'.$bx, true)); ?>
        <?php while ($bxquery->have_posts()) : $bxquery->the_post(); ?>
        <div class="one_fourth <?php if ($bx%4==0) {
                    ?>last_column<?php
                } ?>">
            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            <a href="<?php the_permalink(); ?>"><h3><?php the_title(); ?></h3></a>
            <?php the_content(); ?>
            <a href="<?php the_permalink(); ?>"><p class="ReadMore"><?php esc_attr_e('Read More', 'lodgexyz'); ?></p></a>
        </div>
        <!-- feature-box -->
        <?php if ($bx%4==0) {
                    ?>
        <div class="clear"></div>
        <?php
                } ?>
        <?php endwhile; ?>
        <?php wp_reset_postdata(); ?>
        <?php
            } else {
                ?>
        <!-- feature-box -->
        <?php if ($bx%4==0) {
                    ?>
        <div class="clear"></div>
        <?php
                } ?>
                <?php
            } ?>
            <?php
        }
            /* Home Four Boxes */
        ?>
               </div><!-- services-wrap-->

               <?php
        } ?>

              <div class="clear"></div>
            </div><!-- container -->
       </section>
       <div class="clear"></div>
<?php
    } elseif (is_home()) {
        ?>
<section id="home_slider" style="display:none;"></section>
<section id="wrapsecond" style="display:none;"></section>
<?php
    }
?>
