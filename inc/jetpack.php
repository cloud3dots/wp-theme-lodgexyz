<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package Cloud3Dots LodgeXYZ
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function lodgexyz_jetpack_setup()
{
    add_theme_support('infinite-scroll', array(
        'container' => 'main',
        'footer'    => 'page',
    ));
}
add_action('after_setup_theme', 'lodgexyz_jetpack_setup');
