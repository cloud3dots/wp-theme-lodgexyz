<?php
/**
 * LodgeXYZ Theme Customizer
 *
 * @package Cloud3Dots LodgeXYZ
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function lodgexyz_customize_register($wp_customize) {

    //Add a class for titles
    class XYZ_Lodge_Info extends WP_Customize_Control {
        public $type = 'info';
        public $label = '';
        public function render_content() {
        ?>
            <h3 style="text-decoration: underline; color: #DA4141; text-transform: uppercase;"><?php echo esc_html($this->label); ?></h3>
        <?php
        }
    }

    class WP_Customize_Textarea_Control extends WP_Customize_Control {
        public $type = 'textarea';

        public function render_content() {
        ?>
            <label>
                <span class="customize-control-title"><?php echo esc_html($this->label); ?></span>
                <textarea rows="5" style="width:100%;" <?php $this->link(); ?>><?php echo esc_textarea($this->value()); ?></textarea>
            </label>
        <?php
        }
    }

    $wp_customize->get_setting('blogname')->transport         = 'postMessage';
    $wp_customize->get_setting('blogdescription')->transport  = 'postMessage';
    $wp_customize->remove_control('header_textcolor');
    //$wp_customize->remove_control('display_header_text');

    $wp_customize->add_setting(
        'lodgexyz_options[logo-info]',
        array(
            'type' => 'info_control',
            'capability' => 'edit_theme_options',
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(
        new XYZ_Lodge_Info($wp_customize, 'logo_section',
        array(
            'section' => 'logo_sec',
            'settings' => 'lodgexyz_options[logo-info]',
            'priority' => null
        ))
    );
    $wp_customize->add_setting(
        'color_scheme',
        array(
            'default'	=> '#e75300',
            'sanitize_callback'	=> 'sanitize_hex_color'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Color_Control($wp_customize, 'color_scheme', array(
            'label' => __('Color Scheme', 'lodgexyz'),
            'description' => '',
            'section' => 'colors',
            'settings' => 'color_scheme'
        ))
    );

    // Announcement Section
    $wp_customize->add_section( 'announcement_section',
        array(
            'title' => __('Homepage Announcement', 'lodgexyz'),
            'description' => __('Define a Featured Announcement', 'lodgexyz'),
            'priority' => null
        )
    );
    // Announcement Enabler.
    $wp_customize->add_setting(
        'announcement_enabled',
        array(
            'default' => '0',
            'sanitize_callback' => 'lodgexyz_sanitize_checkbox',
        )
    );
    $wp_customize->add_control(
        'announcement_enabled',
        array(
            'label' => __('Enable anouncement', 'lodgexyz'),
            'section' => 'announcement_section',
            'setting' => 'announcement_enabled',
            'type' => 'checkbox',
            'std' => '0'
        )
    );
    // Announcement.
    $wp_customize->add_setting(
        'announcement_text',
        array(
            'default' => __('Howdy, check this out!', 'lodgexyz'),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'announcement_text',
        array(
            'label' => __('Announcement', 'lodgexyz'),
            'section' => 'announcement_section',
            'setting' => 'announcement_text'
        )
    );


    // Slider Section
    $wp_customize->add_section(
        'slider_section',
        array(
            'title' => __('Homepage Slider', 'lodgexyz'),
            'description' => sprintf(
                __('Slider Image Dimensions (1400 X 532)<br/> Select Featured Image for these pages <br /> How to set featured image <a href="%1$s" target="_blank">Click Here ?</a>', 'lodgexyz'),
                esc_url('"'.CLOUD3DOTS_THEME_FEATURED_SET_VIDEO_URL.'"')
            ),
            'priority' => null
        )
    );
    // Slider Overlay
    $wp_customize->add_setting(
        'slider_overlay_enabled',
        array(
            'default' => '1',
            'sanitize_callback' => 'lodgexyz_sanitize_checkbox',
        )
    );
    $wp_customize->add_control(
        'slider_overlay_enabled',
        array(
            'label' => __('Display slider overlay', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_overlay_enabled',
            'type' => 'checkbox',
            'std' => '1'
        )
    );

    // Slider Image 1
    $wp_customize->add_setting(
        'slider_image1',
        array(
            'default' => get_template_directory_uri().'/assets/images/slides/slider1.png',
            'sanitize_callback' => 'esc_url_raw',
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control($wp_customize, 'slider_image1', array(
            'label' => __('Slider Image 1 (1400 x 532)', 'lodgexyz'),
            'section' => 'slider_section',
            'settings' => 'slider_image1'
        ))
    );
    $wp_customize->add_setting(
        'slider_title1',
        array(
            'default' => __('Celebrating 300 Years of Freemasonry', 'lodgexyz'),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'slider_title1',
        array(
            'label' => __('Slider title 1', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_title1'
        )
    );
    $wp_customize->add_setting(
        'slider_desc1',
        array(
            'default' => __("It's 300 years since four London Lodges met to establish the world's first Grand Lodge for Freemasons. Today there are over 6.5 million freemasons worldwide.", 'lodgexyz'),
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Textarea_Control($wp_customize, 'slider_desc1', array(
            'label' => __('Slider description 1', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_desc1'
        ))
    );
    $wp_customize->add_setting(
        'slider_link1',
        array(
            'default' => '#link1',
            'sanitize_callback' => 'esc_url_raw'
        )
    );

    $wp_customize->add_control(
        'slider_link1',
        array(
            'label' => __('Slider link 1', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_link1'
        )
    );
    // Slider Image 2
    $wp_customize->add_setting(
        'slider_image2',
        array(
            'default' => get_template_directory_uri().'/assets/images/slides/slider2.png',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Image_Control($wp_customize, 'slider_image2', array(
            'label' => __('Slider Image 2 (1400 x 532)', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_image2'
        ))
    );
    $wp_customize->add_setting(
        'slider_title2',
        array(
            'default' => __('Celebrating 300 Years of Freemasonry', 'lodgexyz'),
            'sanitize_callback' => 'sanitize_text_field',
        )
    );
    $wp_customize->add_control(
        'slider_title2',
        array(
            'label' => __('Slider title 2', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_title2'
        )
    );
    $wp_customize->add_setting(
        'slider_desc2',
        array(
            'default' => __("It's 300 years since four London Lodges met to establish the world's first Grand Lodge for Freemasons. Today there are over 6.5 million freemasons worldwide.", 'lodgexyz'),
            'sanitize_callback' => 'sanitize_text_field'
        )
    );
    $wp_customize->add_control(
        new WP_Customize_Textarea_Control($wp_customize, 'slider_desc2', array(
            'label' => __('Slider description 2', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_desc2'
        ))
    );
    $wp_customize->add_setting(
        'slider_link2',
        array(
            'default' => '#link2',
            'sanitize_callback' => 'esc_url_raw'
        )
    );
    $wp_customize->add_control(
        'slider_link2',
        array(
            'label' => __('Slider link 2', 'lodgexyz'),
            'section' => 'slider_section',
            'setting' => 'slider_link2'
      )
    );
    // Slider Image 3
    $wp_customize->add_setting(
        'slider_image3',
        array(
        'default' => get_template_directory_uri().'/assets/images/slides/slider3.png',
        'sanitize_callback' => 'esc_url_raw'
      )
    );

    $wp_customize->add_control(
        new WP_Customize_Image_Control($wp_customize, 'slider_image3', array(
        'label' => __('Slider Image 3 (1400 x 532)', 'lodgexyz'),
        'section' => 'slider_section',
        'setting' => 'slider_image3'
      ))
    );
    $wp_customize->add_setting(
        'slider_title3',
        array(
        'default' => __('Celebrating 300 Years of Freemasonry', 'lodgexyz'),
        'sanitize_callback' => 'sanitize_text_field'
      )
    );
    $wp_customize->add_control(
        'slider_title3',
        array(
        'label' => __('Slider title 3', 'lodgexyz'),
        'section' => 'slider_section',
        'setting' => 'slider_title3'
      )
    );
    $wp_customize->add_setting(
        'slider_desc3',
        array(
        'default' => __("It's 300 years since four London Lodges met to establish the world's first Grand Lodge for Freemasons. Today there are over 6.5 million freemasons worldwide.", 'lodgexyz'),
        'sanitize_callback' => 'sanitize_text_field'
      )
    );
    $wp_customize->add_control(
        new WP_Customize_Textarea_Control($wp_customize, 'slider_desc3', array(
        'label' => __('Slider Description 3', 'lodgexyz'),
        'section' => 'slider_section',
        'setting' => 'slider_desc3'
      ))
    );
    $wp_customize->add_setting(
        'slider_link3',
        array(
        'default' => '',
        'sanitize_callback' => 'esc_url_raw'
      )
    );
    $wp_customize->add_control(
        'slider_link3',
        array(
        'label' => __('Slider link 3', 'lodgexyz'),
        'section' => 'slider_section',
        'setting' => 'slider_link3'
      )
    );

    // Home Featured
    $wp_customize->add_section('page_featured', array(
        'title'	=> __('Homepage Featured', 'lodgexyz'),
        'description' => sprintf(
            __('Featured Image Dimensions : ( 296 X 415 )<br/> Select Featured Image for these pages <br /> How to set featured image %s', 'lodgexyz'),
            sprintf(
                '<a href="%1$s" target="_blank">%2$s</a>',
                esc_url('"'.CLOUD3DOTS_THEME_FEATURED_SET_VIDEO_URL.'"'),
                __('Click Here ?', 'lodgexyz')
                        )
                    ),
        'priority'	=> null
    ));

    $wp_customize->add_setting('page_featured_enabled', array(
        'default'	=> '1',
        'sanitize_callback' => 'lodgexyz_sanitize_checkbox',
    ));
    $wp_customize->add_control('page_featured_enabled', array(
        'label'	=> __('Display featured page', 'lodgexyz'),
        'section'	=> 'page_featured',
        'setting'	=> 'page_featured_enabled',
        'type' => 'checkbox',
        'std' => '1'
    ));

    $wp_customize->add_setting('page_featured_word_limit', array(
        'default'	=> '160',
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page_featured_word_limit', array(
        'label'	=> __('Word limit', 'lodgexyz'),
        'section'	=> 'page_featured',
        'setting'	=> 'page_featured_word_limit'
    ));

    $wp_customize->add_setting('page_featured_setting', array(
        'sanitize_callback'	=> 'lodgexyz_sanitize_integer'
    ));

    $wp_customize->add_control('page_featured_setting', array(
        'type'	=> 'dropdown-pages',
        'label'	=> __('Select page for featured page:', 'lodgexyz'),
        'section'	=> 'page_featured'
    ));

    $wp_customize->add_setting('page_featured_button', array(
        'default'	=> __('Learn More', 'lodgexyz'),
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page_featured_button', array(
        'label'	=> __('Call to action text', 'lodgexyz'),
        'section'	=> 'page_featured',
        'setting'	=> 'page_featured_button'
    ));

    // Home Boxes
    $wp_customize->add_section('page_boxes', array(
        'title'	=> __('Homepage Boxes', 'lodgexyz'),
        'description' => sprintf(
            __('Featured Image Dimensions : ( 830 X 415 )<br/> Select Featured Image for these pages <br /> How to set featured image %s', 'lodgexyz'),
            sprintf(
                '<a href="%1$s" target="_blank">%2$s</a>',
                esc_url('"'.CLOUD3DOTS_THEME_FEATURED_SET_VIDEO_URL.'"'),
                __('Click Here ?', 'lodgexyz')
                        )
                    ),
        'priority'	=> null
    ));

    $wp_customize->add_setting('page_boxes_word_limit', array(
        'default'	=> '45',
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page_boxes_word_limit', array(
        'label'	=> __('Word limit', 'lodgexyz'),
        'section'	=> 'page_boxes',
        'setting'	=> 'page_boxes_word_limit'
    ));

    $wp_customize->add_setting('page-setting1', array(
        'sanitize_callback'	=> 'lodgexyz_sanitize_integer'
    ));

    $wp_customize->add_control('page-setting1', array(
        'type'	=> 'dropdown-pages',
        'label'	=> __('Select page for box one:', 'lodgexyz'),
        'section'	=> 'page_boxes'
    ));

    $wp_customize->add_setting('page-icon1', array(
        'default'	=> 'fa fa-desktop',
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page-icon1', array(
        'label'	=> __('Select icon for box one:', 'lodgexyz'),
        'section'	=> 'page_boxes',
        'setting'	=> 'page-icon1',
        'input_attrs'=> array('class'=>'icp icp-auto', 'data-input-search'=>'true'),
    ));

    $wp_customize->add_setting('page-setting2', array(
        'sanitize_callback'	=> 'lodgexyz_sanitize_integer'
    ));

    $wp_customize->add_control('page-setting2', array(
        'type'	=> 'dropdown-pages',
        'label'	=> __('Select page for box two:', 'lodgexyz'),
        'section'	=> 'page_boxes'
    ));

    $wp_customize->add_setting('page-icon2', array(
        'default'	=> 'fa fa-code',
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page-icon2', array(
        'label'	=> __('Select icon for box two:', 'lodgexyz'),
        'section'	=> 'page_boxes',
        'setting'	=> 'page-icon2',
        'input_attrs'=> array('class'=>'icp icp-auto', 'data-input-search'=>'true'),
    ));

    $wp_customize->add_setting('page-setting3', array(
        'sanitize_callback'	=> 'lodgexyz_sanitize_integer'
    ));

    $wp_customize->add_control('page-setting3', array(
        'type'	=> 'dropdown-pages',
        'label'	=> __('Select page for box three:', 'lodgexyz'),
        'section'	=> 'page_boxes'
    ));

    $wp_customize->add_setting('page-icon3', array(
        'default'	=> 'fa fa-mobile',
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page-icon3', array(
        'label'	=> __('Select icon for box one:', 'lodgexyz'),
        'section'	=> 'page_boxes',
        'setting'	=> 'page-icon3',
        'input_attrs'=> array('class'=>'icp icp-auto', 'data-input-search'=>'true'),
    ));

    $wp_customize->add_setting('page-setting4', array(
        'sanitize_callback'	=> 'lodgexyz_sanitize_integer'
    ));

    $wp_customize->add_control('page-setting4', array(
        'type'	=> 'dropdown-pages',
        'label'	=> __('Select page for box four:', 'lodgexyz'),
        'section'	=> 'page_boxes'
    ));

    $wp_customize->add_setting('page-icon4', array(
        'default'	=> 'fa fa-eye',
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page-icon4', array(
        'label'	=> __('Select icon for box one:', 'lodgexyz'),
        'section'	=> 'page_boxes',
        'setting'	=> 'page-icon4',
        'input_attrs'=> array('class'=>'icp icp-auto', 'data-input-search'=>'true'),
    ));

    // Home Posts
    $wp_customize->add_section('page_posts', array(
        'title'	=> __('Homepage Posts', 'lodgexyz'),
        'description' => sprintf(
            __('Featured Image Dimensions : ( 830 X 415 )<br/> Select Featured Image for these pages <br /> How to set featured image %s', 'lodgexyz'),
            sprintf(
                '<a href="%1$s" target="_blank">%2$s</a>',
                esc_url('"'.CLOUD3DOTS_THEME_FEATURED_SET_VIDEO_URL.'"'),
                __('Click Here ?', 'lodgexyz')
                        )
                    ),
        'priority'	=> null
    ));

    $wp_customize->add_setting('page_posts_category', array(
        'sanitize_callback'	=> 'sanitize_text_field'
    ));

    $wp_customize->add_control('page_posts_category', array(
        'label'	=> __('Category for front page:', 'lodgexyz'),
        'description' => __('Use the category slug', 'lodgexyz'),
        'section'	=> 'page_posts'
    ));

    // Social Settings
    $wp_customize->add_section('social_sec', array(
        'title'	=> __('Social Settings', 'lodgexyz'),
        'description' => sprintf(__('Add social icons link here.', 'lodgexyz')),
        'priority'		=> null
    ));
    $wp_customize->add_setting('fb_link', array(
        'default'	=> '#facebook',
        'sanitize_callback'	=> 'esc_url_raw'
    ));

    $wp_customize->add_control('fb_link', array(
        'label'	=> __('Add facebook link here', 'lodgexyz'),
        'section'	=> 'social_sec',
        'setting'	=> 'fb_link'
    ));
    $wp_customize->add_setting('twitt_link', array(
        'default'	=> '#twitter',
        'sanitize_callback'	=> 'esc_url_raw'
    ));

    $wp_customize->add_control('twitt_link', array(
        'label'	=> __('Add twitter link here', 'lodgexyz'),
        'section'	=> 'social_sec',
        'setting'	=> 'twitt_link'
    ));
    $wp_customize->add_setting('gplus_link', array(
        'default'	=> '#gplus',
        'sanitize_callback'	=> 'esc_url_raw'
    ));
    $wp_customize->add_control('gplus_link', array(
        'label'	=> __('Add google plus link here', 'lodgexyz'),
        'section'	=> 'social_sec',
        'setting'	=> 'gplus_link'
    ));
    $wp_customize->add_setting('linked_link', array(
        'default'	=> '#linkedin',
        'sanitize_callback'	=> 'esc_url_raw'
    ));
    $wp_customize->add_control('linked_link', array(
        'label'	=> __('Add linkedin link here', 'lodgexyz'),
        'section'	=> 'social_sec',
        'setting'	=> 'linked_link'
    ));

    $wp_customize->add_section('footer_text', array(
        'title'	=> __('Footer Area', 'lodgexyz'),
        'priority'	=> null,
        'description'	=> __('Add footer copyright text', 'lodgexyz')
    ));

    $wp_customize->add_setting('about_title', array(
        'default'	=> __('About Us', 'lodgexyz'),
        'sanitize_callback'	=> 'sanitize_text_field'
    ));
    $wp_customize->add_control('about_title', array(
        'label'	=> __('Add about title here', 'lodgexyz'),
        'section'	=> 'footer_text',
        'setting'	=> 'about_title'
    ));

    $wp_customize->add_setting('about_description', array(
        'default'	=> __('Sed suscipit mauris nec mauris vulputate, a posuere libero congue. Nam laoreet elit eu erat pulvinar, et efficitur nibh euismod.Nam metus lorem, hendrerit quis ante eget, lobortis elementum neque. Aliquam in ullamcorper quam. Integer euismod ligula in mauris vehic.', 'lodgexyz'),
        'sanitize_callback'	=> 'sanitize_text_field'
    ));
    $wp_customize->add_control(
        new WP_Customize_Textarea_Control(
            $wp_customize,
            'about_description',
            array(
                'label'	=> __('Add about description for footer', 'lodgexyz'),
                'section'	=> 'footer_text',
                'setting'	=> 'about_description'
            )
        )
    );

    $wp_customize->add_setting('copyright_rights', array(
        'default'	=> '1',
        'sanitize_callback' => 'lodgexyz_sanitize_checkbox',
    ));
    $wp_customize->add_control('copyright_rights', array(
        'label'	=> __('Display copyright', 'lodgexyz'),
        'section'	=> 'footer_text',
        'setting'	=> 'copyright_rights',
        'type' => 'checkbox',
        'std' => '1'
    ));

    $wp_customize->add_setting('copyright_credits', array(
        'default'	=> '1',
        'sanitize_callback' => 'lodgexyz_sanitize_checkbox',
    ));
    $wp_customize->add_control('copyright_credits', array(
        'label'	=> __('Display credits', 'lodgexyz'),
        'section'	=> 'footer_text',
        'setting'	=> 'copyright_credits',
        'type' => 'checkbox',
        'std' => '1'
    ));

    $wp_customize->add_section('contact_sec', array(
        'title'	=> __('Contact Details', 'lodgexyz'),
        'description'	=> __('Add you contact details here', 'lodgexyz'),
        'priority'	=> null
    ));
    $wp_customize->add_setting('contact_sch1', array(
        'default'	=> __('Meets 4th Fri, 7:30pm', 'lodgexyz'),
        'sanitize_callback'	=> 'sanitize_text_field'
    ));
    $wp_customize->add_control('contact_sch1', array(
        'label'	=> __('Add your reglar schedule line 1.', 'lodgexyz'),
        'section'	=> 'contact_sec',
        'setting'	=> 'contact_sch1'
    ));
    $wp_customize->add_setting('contact_sch2', array(
        'default'	=> 'Except Jun, Jul and Aug',
        'sanitize_callback'	=> 'sanitize_text_field'
    ));
    $wp_customize->add_control('contact_sch2', array(
        'label'	=> __('Add your reglar schedule line 2.', 'lodgexyz'),
        'section'	=> 'contact_sec',
        'setting'	=> 'contact_sch2'
    ));
    $wp_customize->add_setting('contact_add1', array(
        'default'	=> __('Athens Masonic Centre', 'lodgexyz'),
        'sanitize_callback'	=> 'sanitize_text_field'
    ));
    $wp_customize->add_control('contact_add1', array(
        'label'	=> __('Add contact address line 1 here.', 'lodgexyz'),
        'section'	=> 'contact_sec',
        'setting'	=> 'contact_add1'
    ));
    $wp_customize->add_setting('contact_add2', array(
        'default'	=> __('69 Keas Road, Athens, Chalandri', 'lodgexyz'),
        'sanitize_callback'	=> 'sanitize_text_field'
    ));
    $wp_customize->add_control('contact_add2', array(
        'label'	=> __('Add contact address line 2 here.', 'lodgexyz'),
        'section'	=> 'contact_sec',
        'setting'	=> 'contact_add2'
    ));
    $wp_customize->add_setting('contact_no', array(
        'default'	=> __('+30-2106019311', 'lodgexyz'),
        'sanitize_callback'	=> 'sanitize_text_field'
    ));
    $wp_customize->add_control('contact_no', array(
        'label'	=> __('Add contact number here.', 'lodgexyz'),
        'section'	=> 'contact_sec',
        'setting'	=> 'contact_no'
    ));
    $wp_customize->add_setting('contact_mail', array(
        'default'	=> 'contact@example.com',
        'sanitize_callback'	=> 'sanitize_email'
    ));
    $wp_customize->add_control('contact_mail', array(
        'label'	=> __('Add you email here', 'lodgexyz'),
        'section'	=> 'contact_sec',
        'setting'	=> 'contact_mail'
    ));
}
add_action('customize_register', 'lodgexyz_customize_register');

// Integer sanitization function.
function lodgexyz_sanitize_integer($input)
{
    if (is_numeric($input)) {
        return intval($input);
    }
}

// Checkbox sanitization function.
function lodgexyz_sanitize_checkbox($checked)
{
    // Boolean check.
    return (isset($checked) && true == $checked);
}

function lodgexyz_custom_css()
{
    ?>
  <style type="text/css">
    a, .header .header-inner .nav ul li a:hover,
    .signin_wrap a:hover,
    .header .header-inner .nav ul li.current_page_item a,
    .header .header-inner .logo h1,
    .header .header-inner .logo span,
    .header .header-inner .logo .c3d,
    #wrapfirst .ReadMore:hover,
    .services-wrap .one_fourth:hover .ReadMore,
    .services-wrap .one_fourth:hover h3,
    .services-wrap .one_fourth:hover .fa,
    .services-wrap .one_fourth:hover .fam,
    .services-wrap .one_fourth:hover .c3d,
    .blog_lists h2 a:hover,
    .blog_lists .c3d,
    #sidebar ul li a:hover,
    .recent-post h6:hover,
    .MoreLink:hover,
    .ReadMore a:hover,
    .services-wrap .one_fourth:hover .ReadMore a,
    .cols-3 ul li a:hover, .cols-3 ul li.current_page_item a
      { color:<?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?>;}

    #wrapfirst .CallToAction,
    .social-icons a:hover,
    .pagination ul li .current, .pagination ul li a:hover,
    #commentform input#submit:hover,
    section#wrapwidgets,
    .nivo-controlNav a.active,
    #slider-overlay
      { background-color:<?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?>;}

    #wrapfirst .ReadMore:hover,
    .services-wrap .one_fourth:hover .ReadMore,
    .services-wrap .one_fourth:hover .fa,
    .services-wrap .one_fourth:hover .fam,
    .services-wrap .one_fourth:hover .c3d,
    .MoreLink:hover
      { border-color:<?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?>;}
  </style>
<?php
}

add_action('wp_head', 'lodgexyz_custom_css');

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function lodgexyz_customize_preview_js()
{
    wp_enqueue_script('lodgexyz_customizer', get_template_directory_uri() . '/assets/js/customizer.js', array( 'customize-preview' ), '20130508', true);
}
add_action('customize_preview_init', 'lodgexyz_customize_preview_js');


function lodgexyz_custom_customize_enqueue()
{
    wp_enqueue_style('lodgexyz-cloud3dots-style', get_template_directory_uri().'/assets/css/cloud3dots.css');
    wp_enqueue_style('lodgexyz-iconpicker-style', get_template_directory_uri().'/assets/css/fontawesome-iconpicker.css');
    wp_enqueue_script('lodgexyz_custom-customize', get_template_directory_uri() . '/assets/js/custom.customize.js', array( 'jquery', 'customize-controls' ), false, true);
    wp_enqueue_script('lodgexyz-iconpicker_js', get_template_directory_uri() . '/assets/js/fontawesome-iconpicker.js', array( 'jquery' ));
}
add_action('customize_controls_enqueue_scripts', 'lodgexyz_custom_customize_enqueue');
