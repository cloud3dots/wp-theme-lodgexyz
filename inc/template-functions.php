<?php

/**
 * Include the TGM_Plugin_Activation class.
 *
 */
require_once get_template_directory() . '/inc/tgm.php';

/**
 * Register the required plugins for this theme.
 */
function lodgexyz_register_required_plugins()
{
    $plugins = array(

        // Until this theme is published to the Wordpress Marketplace, the use of Update Checker for getting periodic updates is highly recommended.
        array(
            'name'         => 'Update Checker',
            'slug'         => 'update-checker',
            'source'       => 'https://gitlab.com/cloud3dots/wp-plugin-update-checker/-/archive/master/wp-plugin-update-checker-master.zip',
            'required'     => false,
            'external_url' => 'https://gitlab.com/cloud3dots/wp-plugin-update-checker',
        ),

        // Some Cloud3Dots icons are included but installing the full package is recommended.
        array(
            'name'         => 'Icons Cloud3Dots',
            'slug'         => 'icons-cloud3dots',
            'source'       => 'https://gitlab.com/cloud3dots/wp-plugin-icons-cloud3dots/-/archive/master/wp-plugin-icons-cloud3dots-master.zip',
            'required'     => false,
            'external_url' => 'https://gitlab.com/cloud3dots/wp-plugin-icons-cloud3dots',
        ),

        // For using masonic icons, installing the Plugin FAMCode Icons is highly recommended.
        array(
            'name'         => 'Icons FAMCode',
            'slug'         => 'icons-famcode',
            'source'       => 'https://gitlab.com/cloud3dots/wp-plugin-icons-famcode/-/archive/master/wp-plugin-icons-famcode-master.zip',
            'required'     => false,
            'external_url' => 'https://gitlab.com/cloud3dots/wp-plugin-icons-famcode',
        ),

        // Other recommended plugins from the WordPress Plugin Repository.
        array(
            'name'      => esc_html__('Contact Form 7', 'LodgeXYZ'),
            'slug'      => 'contact-form-7',
            'required'  => false,
        ),

        array(
            'name'      => esc_html__('BuddyPress', 'LodgeXYZ'),
            'slug'      => 'buddypress',
            'required'  => false,
        ),

        array(
            'name'      => esc_html__('WordPress SEO by Yoast', 'LodgeXYZ'),
            'slug'      => 'wordpress-seo',
            'required'  => false,
            'is_callable' => 'wpseo_init',
        ),

        array(
            'name'      => esc_html__('iThemes Security', 'LodgeXYZ'),
            'slug'      => 'better-wp-security',
            'required'  => false,
        ),

        array(
            'name'      => esc_html__('One Click Demo Import', 'LodgeXYZ'),
            'slug'      => 'one-click-demo-import',
            'required'  => false,
        ),
    );

    /*
     * Array of configuration settings..
     *
     */
    $config = array(
        'id'           => 'lodgexyz',              // Unique ID for hashing notices for multiple instances of TGMPA.
        'default_path' => '',                      // Default absolute path to bundled plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'parent_slug'  => 'themes.php',            // Parent menu slug.
        'capability'   => 'edit_theme_options',    // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
    );

    tgmpa($plugins, $config);
}
add_action('tgmpa_register', 'lodgexyz_register_required_plugins');

function lodgexyz_ocdi_import_files()
{
    return array(
        array(
            'import_file_name'             => 'Lodge XYZ Demo Import',
            'local_import_file'            => trailingslashit(get_template_directory()) . 'assets/dummy-data/demo-content.xml',
            'local_import_widget_file'     => trailingslashit(get_template_directory()) . 'assets/dummy-data/widgets.wie',
            'local_import_customizer_file' => trailingslashit(get_template_directory()) . 'assets/dummy-data/customizer.dat'
        )
    );
}
add_filter('pt-ocdi/import_files', 'lodgexyz_ocdi_import_files');
