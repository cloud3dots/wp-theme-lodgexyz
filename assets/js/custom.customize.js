jQuery(document).ready(function(e) {

  $('.icp-auto').iconpicker({
    selectedCustomClass: 'label label-success',
    placement: 'bottomRight',
    hideOnSelect: true,
  }).data('iconpicker').show();

  $('.icp').on('iconpickerSelected', function(e) {
    $(this).change();
  });

});
