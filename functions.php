<?php
/**
 * LodgeXYZ functions and definitions
 *
 * @package Cloud3Dots LodgeXYZ
 */


// Set the word limit of post content

function lodgexyz_limit_the_content($content)
{
    $word_limit = 1000;
    $words = explode(' ', $content);
    if (!is_single() && !is_page()) {
        return implode(' ', array_slice($words, 0, $word_limit));
    }
    return $content;
}
add_action('the_content', 'lodgexyz_limit_the_content');

function lodgexyz_short_the_content($content, $word_limit = 30)
{
    $words = explode(' ', $content);
    return implode(' ', array_slice($words, 0, $word_limit));
}

/**
 * Set the content width based on the theme's design and stylesheet.
 */

if (! function_exists('lodgexyz_setup')) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which runs
 * before the init hook. The init hook is too late for some features, such as indicating
 * support post thumbnails.
 */
function lodgexyz_setup()
{
    if (! isset($content_width)) {
        $content_width = 640; /* pixels */
    }

    load_theme_textdomain('lodgexyz', get_template_directory() . '/languages');
    add_theme_support('automatic-feed-links');
    add_theme_support('woocommerce');
    add_theme_support('post-thumbnails');
    set_post_thumbnail_size(825, 510, true);
    add_theme_support('custom-header');
    add_theme_support('title-tag');
    add_theme_support('custom-logo', array(
        'height'      => 25,
        'width'       => 150,
        'flex-height' => true,
        'flex-width'  => true,
        'header-text' => array( 'site-title', 'site-description' ),
    ));
    add_image_size('lodgexyz-homepage-thumb', 240, 145, true);
    register_nav_menus(
        array(
      'primary' => esc_attr__('Primary Menu', 'lodgexyz'),
      'social'  => esc_attr__('Social Links Menu', 'lodgexyz'),
  )
  );
    add_editor_style('editor-style.css');
    add_theme_support('html5', array(
      'search-form',
      'comment-form',
      'comment-list',
      'gallery',
      'caption',
  ));
    add_theme_support('post-formats', array(
      'aside',
      'image',
      'video',
      'quote',
      'link',
      'gallery',
      'status',
      'audio',
      'chat',
  ));
    add_theme_support('custom-background', array(
        'default-color' => 'ffffff'
    ));
    add_theme_support('customize-selective-refresh-widgets');
}
endif; // lodgexyz_setup
add_action('after_setup_theme', 'lodgexyz_setup');

function lodgexyz_widgets_init()
{
    register_sidebar(array(
        'name'          => esc_attr__('Blog Sidebar', 'lodgexyz'),
        'description'   => esc_attr__('Appears on blog page sidebar', 'lodgexyz'),
        'id'            => 'sidebar-1',
        'before_widget' => '',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3><aside id="%1$s" class="widget %2$s">',
        'after_widget'  => '</aside>',
    ));
}
add_action('widgets_init', 'lodgexyz_widgets_init');


function lodgexyz_font_url()
{
    $font_url = '';

    /* Translators: If there are any character that are not
    * supported by Oswald, trsnalate this to off, do not
    * translate into your own language.
    */
    $roboto = _x('on', 'Roboto:on or off', 'lodgexyz');

    /* Translators: If there has any character that are not supported
    *  by Scada, translate this to off, do not translate
    *  into your own language.
    */

    if ('off' !== $roboto) {
        $font_family = array();

        if ('off' !== $roboto) {
            $font_family[] = 'Roboto:300,400,600,700,800,900';
        }
        $query_args = array(
                'family'	=> urlencode(implode('|', $font_family)),
            );

        $font_url = add_query_arg($query_args, '//fonts.googleapis.com/css');
    }

    return $font_url;
}


function lodgexyz_scripts()
{
    wp_enqueue_style('lodgexyz-font', lodgexyz_font_url(), array());
    wp_enqueue_style('lodgexyz-basic-style', get_stylesheet_uri());
    wp_enqueue_style('lodgexyz-editor-style', get_template_directory_uri().'/editor-style.css');
    wp_enqueue_style('lodgexyz-nivoslider-style', get_template_directory_uri().'/assets/css/nivo-slider.css');
    wp_enqueue_style('lodgexyz-main-style', get_template_directory_uri().'/assets/css/responsive.css');
    wp_enqueue_style('lodgexyz-base-style', get_template_directory_uri().'/assets/css/style_base.css');
    wp_enqueue_style('lodgexyz-animation-style', get_template_directory_uri().'/assets/css/animation.css');
    wp_enqueue_style('lodgexyz-cloud3dots-style', get_template_directory_uri().'/assets/css/cloud3dots.css');
    wp_enqueue_style('lodgexyz-iconpicker-style', get_template_directory_uri().'/assets/css/fontawesome-iconpicker.css');
    wp_enqueue_script('lodgexyz-nivo-script', get_template_directory_uri() . '/assets/js/jquery.nivo.slider.js', array('jquery'));
    wp_enqueue_script('lodgexyz-custom_js', get_template_directory_uri() . '/assets/js/custom.js');

    if (is_singular() && comments_open() && get_option('thread_comments')) {
        wp_enqueue_script('comment-reply');
    }
}
add_action('wp_enqueue_scripts', 'lodgexyz_scripts');

function lodgexyz_load_dashicons_front_end()
{
    wp_enqueue_style('dashicons');
}
add_action('wp_enqueue_scripts', 'lodgexyz_load_dashicons_front_end');

function lodgexyz_ie_stylesheet()
{
    global $wp_styles;

    /** Load our IE-only stylesheet for all versions of IE.
    *   <!--[if lt IE 9]> ... <![endif]-->
    *
    *  Note: It is also possible to just check and see if the $is_IE global in WordPress is set to true before
    *  calling the wp_enqueue_style() function. If you are trying to load a stylesheet for all browsers
    *  EXCEPT for IE, then you would HAVE to check the $is_IE global since WordPress doesn't have a way to
    *  properly handle non-IE conditional comments.
    */
    wp_enqueue_style('lodgexyz_ie', get_template_directory_uri().'/assets/css/ie.css', array('lodgexyz-style'));
    $wp_styles->add_data('lodgexyz_ie', 'conditional', 'IE');
}
add_action('wp_enqueue_scripts', 'lodgexyz_ie_stylesheet');

define('CLOUD3DOTS_URL', 'https://cloud3dots.com/');
define('CLOUD3DOTS_THEME_URL', 'https://gitlab.com/cloud3dots/lodgexyz');
define('CLOUD3DOTS_THEME_DOC', 'https://gitlab.com/cloud3dots/wp-theme-xyz/wikis/home');
define('CLOUD3DOTS_THEME_FEATURED_SET_VIDEO_URL', 'https://www.youtube.com/watch?v=310YGYtGLIM');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
require get_template_directory() . '/inc/jetpack.php';

// get slug by id
function lodgexyz_get_slug_by_id($id)
{
    $post_data = get_post($id, ARRAY_A);
    $slug = $post_data['post_name'];
    return $slug;
}

if (! function_exists('lodgexyz_the_custom_logo')) :
/**
 * Displays the optional custom logo.
 *
 * Does nothing if the custom logo is not available.
 *
 */
function lodgexyz_the_custom_logo()
{
    if (function_exists('the_custom_logo')) {
        the_custom_logo();
    }
}
endif;

function lodgexyz_get_fa_icon($key)
{
    $icons = array(
    'feature-box-1' => 'fa-desktop',
    'feature-box-2' => 'fa-code',
    'feature-box-3' => 'fa-mobile',
    'feature-box-4' => 'fa-eye'
  );
    return $icons[$key];
}

function lodgexyz_login_logo()
{
    ?>
  <style type="text/css">
    #login form#loginform p.submit input#wp-submit,
    #login form#lostpasswordform p.submit input#wp-submit,
    #login form#registerform p.submit input#wp-submit {
      background-color:<?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?>;
      border-color:<?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?>;
      box-shadow: 0 1px 0 0 <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
      text-shadow: 1px 1px 2px <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
    }
    #login form#loginform p.submit,
    #login form#lostpasswordform p.submit,
    #login form#registerform p.submit {
      box-shadow: 0 1px 0 0 <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
      text-shadow: 1px 1px 2px <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
    }
    #login form#loginform p label input:focus,
    #login form#lostpasswordform p label input:focus,
    #login form#registerform p label input:focus,
    #login form#loginform p label input[type="checkbox"]:checked + label::after,
    #login form#lostpasswordform p label input[type="checkbox"]:checked + label::after,
    #login form#registerform p label input[type="checkbox"]:checked + label::after {
      border-color: <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
      box-shadow: 0 1px 0 0 <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
    }
    #login a:focus {
      outline: 0;
      border: 1px solid <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
      color: <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
      box-shadow: none !important;
    }
    #login a:hover {
      color: <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
    }
    #login p.message {
      border-left: 4px solid <?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?> !important;
    }
  </style>
  <?php
  if ('' != get_theme_mod('custom_logo')) {
      // Show custom logo.
      $custom_logo_id = get_theme_mod('custom_logo');
      $custom_logo_url = esc_url(wp_get_attachment_image_url($custom_logo_id, 'full')); ?>
    <style type="text/css">
      #login h1 a, .login h1 a {
        background-image: url(<?php echo $custom_logo_url; ?>);
        background-repeat: no-repeat;
        width: 320px;
        height: 114px;
      }
    </style>
    <?php
  } else {
      $custom_logo_url = get_stylesheet_directory_uri() . '/assets/images/ffffff-0.png'; ?>
    <style type="text/css">
      #login h1 a, .login h1 a {
        background-image: url(<?php echo $custom_logo_url; ?>);
        background-repeat: no-repeat;
        width: 320px;
        height: 114px;
        text-indent: 0;
        color:<?php echo esc_attr(get_theme_mod('color_scheme', '#e75300')); ?>;
      }
    </style>
    <?php
  }
}
add_action('login_enqueue_scripts', 'lodgexyz_login_logo');

function lodgexyz_login_logo_url()
{
    return home_url();
}
add_filter('login_headerurl', 'lodgexyz_login_logo_url');

function lodgexyz_login_logo_url_title()
{
    return get_bloginfo('name');
}
add_filter('login_headertext', 'lodgexyz_login_logo_url_title');

function lodgexyz_login_stylesheet()
{
    wp_enqueue_style('lodgexyz-basic-style', get_stylesheet_uri());
    wp_enqueue_style('custom-login', get_stylesheet_directory_uri() . '/assets/css/cloud3dots.css');
}
add_action('login_enqueue_scripts', 'lodgexyz_login_stylesheet');

function lodgexyz_login_javascript()
{
    wp_enqueue_script('jquery');
    wp_enqueue_script('custom-login-javascript', get_stylesheet_directory_uri() . '/assets/js/custom.login.js');
}
add_action('login_enqueue_scripts', 'lodgexyz_login_javascript');
